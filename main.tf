provider "aws" {
  region = "eu-central-1"  
}

# Create the S3 bucket
resource "aws_s3_bucket" "assignment-storageefti11" {
  bucket = "assignment-storageefti11"  
  force_destroy = true
  
  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }        

}

/* resource "aws_s3_bucket_acl" "assignment-storageefti11" {
  bucket = aws_s3_bucket.assignment-storageefti11.id
  acl    = "private"
} */