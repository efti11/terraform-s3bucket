FROM python:latest
# set the directory on the container
WORKDIR /app
# copy the python script to the working directory of the container
COPY scripti.py .
# install the required dependencies
RUN pip install boto3
# run the python script
CMD ["python", "scripti.py"]