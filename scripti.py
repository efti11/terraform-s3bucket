import boto3;

def enumerate_objects(bucket_name):
  


    # access the aws resources - in our case the s3 bucket-
    s3_client = boto3.client('s3')
    response = s3_client.list_objects_v2(Bucket=bucket_name)

    # print the object names of the S3 Bucket
    if 'Contents' in response:
        for obj in response['Contents']:
            print(obj['Key'])
    else:
        print("No objects found in the bucket.")


enumerate_objects('assignment-storageefti11')